/**
 * Koios. Artificial Inteligence library.
 * 
 * @copyright (C) 2014 David Henar Palacin.
 * @license GNU General Public License version 3 or later, see license.txt
 */

package org.shadoware.koios.dice.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.shadoware.koios.dice.Dice;


public class DiceTest {
	
	private final int MIN_ITERATIONS = 10;
	
	private enum EnumValues {
		ONE, TWO, THREE
	}	
	
	@Test
	public void enumType() {
		Dice<EnumValues> literalDice = new Dice<EnumValues>(EnumValues.values());
		boolean reachONE = false;
		boolean reachTWO = false;
		boolean reachTHREE = false;
		
		for (int i = 0; i < 30; i++) {
			EnumValues result = literalDice.roll();
			reachONE = (result == EnumValues.ONE) || reachONE;
			reachTWO = (result == EnumValues.TWO) || reachTWO;
			reachTHREE = (result == EnumValues.THREE) || reachTHREE;
		}
		assertTrue(reachONE);
		assertTrue(reachTWO);
		assertTrue(reachTHREE);
	}

	@Test
	public void positiveRoll() {
		Dice d6 = new Dice(1,6);
		testNumericRoll(d6, 1, 6);
		
		d6 = new Dice(6);
		testNumericRoll(d6, 0, 5);
		
		Integer[] numericValues = {1, 2, 3, 4};
		Dice<Integer>d4 = new Dice<Integer>(numericValues);
		testNumericRoll(d4, 1, 4);
	}
	
	@Test
	public void negativeRoll() {
		Dice d6 = new Dice(-3,-10);
		testNumericRoll(d6, 3, 10);
	}
	
	@Test
	public void inverseRoll() {
		Dice d6 = new Dice(7, 0);
		testNumericRoll(d6, 0, 7);
	}
	
	private void testNumericRoll(Dice<Integer> dice, int min, int max) {
		int result;
		boolean reachMin = false;
		boolean reachMax = false;
		
		for (int i = 0; i < getIterations(min, max); i++) {
			result = (Integer) dice.roll();
			reachMin = (result == min) || reachMin;
			reachMax = (result == max) || reachMax;
			assertTrue(result >= min);
			assertTrue(result <= max);
		}
		assertTrue(reachMax);
		assertTrue(reachMin);
	}
	
	private int getIterations(int min, int max) {
		return MIN_ITERATIONS * (Math.abs(max) - Math.abs(min));
	}
}
