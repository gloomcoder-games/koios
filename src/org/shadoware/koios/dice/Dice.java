/**
 * Koios. Artificial Inteligence library.
 * 
 * @copyright (C) 2014 David Henar Palacin.
 * @license GNU General Public License version 3 or later, see license.txt
 */

package org.shadoware.koios.dice;

public class Dice<E> {
	
	private E[] values;
	
	public Dice(E[] values) {
		this.values = values;
	}
	
	public Dice(int range) {
		this(0, (range - 1));
	}
	
	@SuppressWarnings("unchecked")
	public Dice(int limitA, int limitB) {
		int max = Math.max(Math.abs(limitA), Math.abs(limitB));
		int min = Math.min(Math.abs(limitA), Math.abs(limitB));
		Integer[] numericValues = new Integer[getRange(max, min)];
		generateSequence(numericValues, min);
		values = (E[]) numericValues;
	}
	
	private int getRange(int max, int min) {
		return max - min + 1;
	}
	
	private void generateSequence(Integer[] values, int starting) {
		for (int i = 0; i < values.length; i++) {
			values[i] = i + starting;
		}
	}
	
	public E roll() {
		int random = (int) (Math.random() * values.length);
		return values[random];
	}
}